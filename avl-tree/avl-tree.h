#ifndef DATA_STRUCTURES_AVL_TREE_H
#define DATA_STRUCTURES_AVL_TREE_H

#include <stddef.h>

#include "node/node.h"

typedef struct AvlTree {
	AT_Node *root;
	size_t dataSize;
	size_t size;
	int (*compare)(void *data1, void *data2);
} AvlTree;

AvlTree *AvlTree_Create(size_t dataSize, int (*compare)(void *data1, void *data2));
void AvlTree_Destroy(AvlTree *tree);
AT_Node *AvlTree_Root(AvlTree *tree);
AT_Node *AvlTree_Search(AvlTree *tree, void *data);
void AvlTree_Insert(AvlTree *tree, void *data);
void AvlTree_Remove(AvlTree *tree, void *data);
void AvlTree_TraversePreorder(AvlTree *tree, void (*function)(void *data));
void AvlTree_TraverseInorder(AvlTree *tree, void (*function)(void *data));
void AvlTree_TraversePostorder(AvlTree *tree, void (*function)(void *data));
void AvlTree_TraverseLevelorder(AvlTree *tree, void (*function)(void *data));

#endif
