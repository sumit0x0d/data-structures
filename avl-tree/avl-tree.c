#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "avl-tree.h"

#include "array/array.h"
#include "circular-buffer/circular-buffer.h"
#include "node/node.h"

static AT_CircularBuffer *circularBuffer;

static size_t getHeight(AT_Node *node);
static void updateBalanceFactor(AT_Node *node);
static void rotateRight(AvlTree *tree, AT_Node *node);
static void rotateLeftRight(AvlTree *tree, AT_Node *node);
static void rotateLeft(AvlTree *tree, AT_Node *node);
static void rotateRightLeft(AvlTree *tree, AT_Node *node);
static void rebalance(AvlTree *tree, AT_Node *node);

AvlTree *AvlTree_Create(size_t dataSize, int (*compare)(void *data1, void *data2))
{
	AvlTree *tree = (AvlTree *)malloc(sizeof (AvlTree));
	assert(tree);

	circularBuffer = (AT_CircularBuffer *)malloc(sizeof (AT_CircularBuffer));
	assert(circularBuffer);

	tree->root = NULL;
	tree->dataSize = dataSize;
	tree->size = 0;
	tree->compare = compare;
	return tree;
}

void AvlTree_Destroy(AvlTree *tree)
{
	AT_Node *node = tree->root;
	AT_CircularBuffer_Create(circularBuffer, tree->size);
	free(node->data);
	AT_CircularBuffer_PushBack(circularBuffer, node);
	while (circularBuffer->size) {
		node = AT_CircularBuffer_GetFront(circularBuffer);
		AT_CircularBuffer_PopFront(circularBuffer);
		if (node->left) {
			free(node->data);
			AT_CircularBuffer_PushBack(circularBuffer, node->left);
		}
		if (node->right) {
			free(node->data);
			AT_CircularBuffer_PushBack(circularBuffer, node->right);
		}
	}
	AT_CircularBuffer_Destroy(circularBuffer);
	free(circularBuffer);
	free(tree);
}

AT_Node *AvlTree_Root(AvlTree *tree)
{
	return tree->root;     ;
}

AT_Node *AvlTree_Search(AvlTree *tree, void *data)
{
	AT_Node *node = tree->root;
	while (node) {
		int compare = tree->compare(data, node->data);
		if (compare == 0) {
			return node;
		} else if (compare < 0) {
			node = node->left;
		} else {
			node = node->right;
		}
	}
	return NULL;
}

void AvlTree_Insert(AvlTree *tree, void *data)
{
	if (!tree->root) {
		tree->root = AT_Node_Create(data, tree->dataSize);
		tree->root->parent = NULL;
		tree->size++;
		return;
	}
	circularBuffer->base = malloc(sizeof (AT_Node *) * ((tree->size + 2) / 2));
	assert(circularBuffer->base);
	AT_Node *node = tree->root;
	AT_Node *nodeParent = NULL;
	int compare;
	while (node) {
		compare = tree->compare(data, node->data);
		if (!compare) {
			free(circularBuffer->base);
			return;
		}
		nodeParent = node;
		if (compare < 0) {
			node = node->left;
		} else {
			node = node->right;
		}
	}
	node = AT_Node_Create(data, tree->dataSize);
	node->parent = nodeParent;
	if (compare < 0) {
		nodeParent->left = node;
	} else {
		nodeParent->right = node;
	}
	rebalance(tree, nodeParent);
	free(circularBuffer->base);
	tree->size++;
}

void AvlTree_Remove(AvlTree *tree, void *data)
{
	AT_CircularBuffer_Create(circularBuffer, ((tree->size + 2) / 2));
	AT_Node *node = tree->root;
	AT_Node *nodeParent = NULL;
	while (node) {
		int compare = tree->compare(data, node->data);
		if (!compare) {
			break;
		}
		nodeParent = node;
		if (compare < 0) {
			node = node->left;
		} else {
			node = node->right;
		}
	}
	if (!node) {
		return;
	}
	if (!node->left && !node->right) {
		if (nodeParent->left == node) {
			nodeParent->left = NULL;
		} else {
			nodeParent->right = NULL;
		}
		AT_Node_Destroy(node);
		rebalance(tree, nodeParent);
	} else if (!node->left) {
		if (nodeParent->right == node) {
			nodeParent->right = node->right;
		} else {
			nodeParent->left = node->right;
		}
		AT_Node_Destroy(node);
		rebalance(tree, nodeParent);
	} else if (!node->right) {
		if (nodeParent->left == node) {
			nodeParent->left = node->left;
		} else {
			nodeParent->right = node->left;
		}
		AT_Node_Destroy(node);
		rebalance(tree, nodeParent);
	} else {
		if (tree->root->balanceFactor < 0) {
			node = AT_Node_GetPredecessor(node);
		} else {
			node = AT_Node_GetSuccessor(node);
		}
		rebalance(tree, node);
	}
	AT_CircularBuffer_Destroy(circularBuffer);
	tree->size--;
}

void AvlTree_TraversePreorder(AvlTree *tree, void (*function)(void *data))
{
	AT_Node *node = tree->root;
	AT_Array *array = AT_Array_Create(tree->size);
	while (node || array->size) {
		if (node) {
			AT_Array_PushBack(array, node);
			function(node->data);
			node = node->left;
		} else {
			node = (AT_Node *)AT_Array_GetBack(array);
			AT_Array_PopBack(array);
			node = node->right;
		}
	}
	AT_Array_Destroy(array);
}

void AvlTree_TraverseInorder(AvlTree *tree, void (*function)(void *data))
{
	AT_Node *node = tree->root;
	AT_Array *array = AT_Array_Create(tree->size);
	while (node || array->size) {
		if (node) {
			AT_Array_PushBack(array, node);
			node = node->left;
		} else {
			node = (AT_Node *)AT_Array_GetBack(array);
			AT_Array_PopBack(array);
			function(node->data);
			node = node->right;
		}
	}
	AT_Array_Destroy(array);
}

void AvlTree_TraversePostorder(AvlTree *tree, void (*function)(void *data))
{
	AT_Node *node = tree->root;
	AT_Array *array = AT_Array_Create(tree->size);
	while (node || array->size) {
		if (node) {
			AT_Array_PushBack(array, node);
			function(node->data);
			node = node->left;
		} else {
			node = (AT_Node *)AT_Array_GetBack(array);
			AT_Array_PopBack(array);
			node = node->right;
		}
	}
	AT_Array_Destroy(array);
}

void AvlTree_TraverseLevelorder(AvlTree *tree, void (*function)(void *data))
{
	AT_Node *node = tree->root;
	AT_CircularBuffer_Create(circularBuffer, tree->size);
	function(node->data);
	AT_CircularBuffer_PushBack(circularBuffer, node);
	while (circularBuffer->size) {
		node = AT_CircularBuffer_GetFront(circularBuffer);
		AT_CircularBuffer_PopFront(circularBuffer);
		if (node->left) {
			function(node->data);
			AT_CircularBuffer_PushBack(circularBuffer, node->left);
		}
		if (node->right) {
			function(node->data);
			AT_CircularBuffer_PushBack(circularBuffer, node->right);
		}
	}
	AT_CircularBuffer_Destroy(circularBuffer);
}

static size_t getHeight(AT_Node *node)
{
	size_t height = 0;
	AT_CircularBuffer_PushBack(circularBuffer, node);
	while (circularBuffer->size) {
		size_t circularBufferSize = circularBuffer->size;
		while (circularBufferSize) {
			node = AT_CircularBuffer_GetFront(circularBuffer);
			AT_CircularBuffer_PopFront(circularBuffer);
			if (node->left) {
				AT_CircularBuffer_PushBack(circularBuffer, node->left);
			}
			if (node->right) {
				AT_CircularBuffer_PushBack(circularBuffer, node->right);
			}
			circularBufferSize--;
		}
		height++;
	}
	return height;
}

static void updateBalanceFactor(AT_Node *node)
{
	size_t nodeLeftHeight = 0;
	size_t nodeRightHeight = 0;
	if (node->left) {
		nodeLeftHeight = getHeight(node->left);
	}
	if (node->right) {
		nodeRightHeight = getHeight(node->right);
	}
	node->balanceFactor = (int)(nodeLeftHeight - nodeRightHeight);
}

static void rotateRight(AvlTree *tree, AT_Node *node)
{
	AT_Node *nodeLeft = node->left;
	node->left = nodeLeft->right;
	if (node->left) {
		node->left->parent = node;
	}
	nodeLeft->parent = node->parent;
	if (nodeLeft->parent) {
		if (nodeLeft->parent->left == node) {
			nodeLeft->parent->left = nodeLeft;
		} else {
			nodeLeft->parent->right = nodeLeft;
		}
	} else {
		tree->root = nodeLeft;
	}
	node->parent = nodeLeft;
	nodeLeft->right = node;
}

static void rotateLeftRight(AvlTree *tree, AT_Node *node)
{
	AT_Node *nodeLeft = node->left;
	AT_Node *nodeLeftRight = node->left->right;
	nodeLeft->right = nodeLeftRight->left;
	if (nodeLeft->right) {
		nodeLeft->right->parent = nodeLeft;
	}
	node->left = nodeLeftRight->right;
	if (node->left) {
		node->left->parent = node;
	}
	nodeLeftRight->left = nodeLeft;
	nodeLeft->parent = nodeLeftRight;
	nodeLeftRight->parent = node->parent;
	if (nodeLeftRight->parent) {
		if (nodeLeftRight->parent->left == node) {
			nodeLeftRight->parent->left = nodeLeftRight;
		} else {
			nodeLeftRight->parent->right = nodeLeftRight;
		}
	} else {
		tree->root = nodeLeftRight;
	}
	node->parent = nodeLeftRight;
	nodeLeftRight->right = node;
}

static void rotateLeft(AvlTree *tree, AT_Node *node)
{
	AT_Node *nodeRight = node->right; 
	node->right = nodeRight->left;
	if (node->right) {
		node->right->parent = node;
	}
	nodeRight->parent = node->parent;
	if (nodeRight->parent) {
		if (nodeRight->parent->left == node) {
			nodeRight->parent->left = nodeRight;
		} else {
			nodeRight->parent->right = nodeRight;
		}
	} else {
		tree->root = nodeRight;
	}
	node->parent = nodeRight;
	nodeRight->left = node;
}

static void rotateRightLeft(AvlTree *tree, AT_Node *node)
{
	AT_Node *nodeRight = node->right;
	AT_Node *nodeRightLeft = node->right->left;
	nodeRight->left = nodeRightLeft->right;
	if (nodeRight->left) {
		nodeRight->left->parent = nodeRight;
	}
	node->right = nodeRightLeft->left;
	if (node->right) {
		node->right->parent = node;
	}
	nodeRightLeft->right = nodeRight;
	nodeRight->parent = nodeRightLeft;
	nodeRightLeft->parent = node->parent;
	if (nodeRightLeft->parent) {
		if (nodeRightLeft->parent->left == node) {
			nodeRightLeft->parent->left = nodeRightLeft;
		} else {
			nodeRightLeft->parent->right = nodeRightLeft;
		}
	} else {
		tree->root = nodeRightLeft;
	}
	node->parent = nodeRightLeft;
	nodeRightLeft->left = node;
}

static void rebalance(AvlTree *tree, AT_Node *node)
{
	while (node) {
		updateBalanceFactor(node);
		if (node->balanceFactor == -2) {
			if (node->left->balanceFactor == -1) {
				rotateRight(tree, node);
				updateBalanceFactor(node);
				updateBalanceFactor(node->right);
			} else if (node->left->balanceFactor == 1) {
				rotateLeftRight(tree, node);
				updateBalanceFactor(node);
			}
		} else if (node->balanceFactor == 2) {
			if (node->right->balanceFactor == 1) {
				rotateLeft(tree, node);
				updateBalanceFactor(node);
				updateBalanceFactor(node->left);
			} else if (node->right->balanceFactor == -1) {
				rotateRightLeft(tree, node);
				updateBalanceFactor(node);
			}
		}
		node = node->parent;
	}
}
