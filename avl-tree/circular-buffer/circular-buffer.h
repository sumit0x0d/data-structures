#ifndef AVL_TREE_CIRCULAR_BUFFER_H
#define AVL_TREE_CIRCULAR_BUFFER_H

#include <stddef.h>

#include "../node/node.h"

typedef struct AT_CircularBuffer {
	AT_Node **base;
	size_t front;
	size_t back;
	size_t capacity;
	size_t size;
} AT_CircularBuffer;

void AT_CircularBuffer_Create(AT_CircularBuffer *array, size_t capacity);
void AT_CircularBuffer_Destroy(AT_CircularBuffer *array);

AT_Node *AT_CircularBuffer_GetFront(AT_CircularBuffer *array);
AT_Node *AT_CircularBuffer_GetBack(AT_CircularBuffer *array);
void AT_CircularBuffer_PushBack(AT_CircularBuffer *array, AT_Node *data);
void AT_CircularBuffer_PopFront(AT_CircularBuffer *array);

#endif
