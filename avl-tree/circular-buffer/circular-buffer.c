#include <assert.h>
#include <stdlib.h>

#include "circular-buffer.h"

void AT_CircularBuffer_Create(AT_CircularBuffer *array, size_t capacity)
{
	array->base = (AT_Node **)malloc(capacity * sizeof (AT_Node *));
	assert(array->base);
	array->front = 0;
	array->back = 0;
	array->capacity = capacity;
	array->size = 0;
}

void AT_CircularBuffer_Destroy(AT_CircularBuffer *array)
{
	free(array->base);
}

AT_Node *AT_CircularBuffer_GetFront(AT_CircularBuffer *array)
{
	return array->base[array->front * sizeof (AT_Node *)];
}

AT_Node *AT_CircularBuffer_GetBack(AT_CircularBuffer *array)
{
	if (array->back == 0) {
		return array->base[(array->capacity - 1) * sizeof (AT_Node *)];
	}
	return array->base[(array->back - 1) * sizeof (AT_Node *)];
}

void AT_CircularBuffer_PushBack(AT_CircularBuffer *array, AT_Node *data)
{
	array->base[array->back * sizeof (AT_Node *)] = data;
	array->back = (array->back + 1) % array->capacity;
	array->size++;
}

void AT_CircularBuffer_PopFront(AT_CircularBuffer *array)
{
	array->front = (array->front + 1) % array->capacity;
	array->size--;
}
