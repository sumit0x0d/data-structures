#include <assert.h>
#include <stdlib.h>

#include "array.h"

AT_Array *AT_Array_Create(size_t capacity)
{
	AT_Array *array = (AT_Array *)malloc(sizeof (AT_Array));
	assert(array);
	array->base = (AT_Node **)malloc(capacity * sizeof (AT_Node *));
	assert(array->base);
	array->capacity = capacity;
	array->size = 0;
	return array;
}

void AT_Array_Destroy(AT_Array *array)
{
	free(array->base);
	free(array);
}

AT_Node *AT_Array_GetFront(AT_Array *array)
{
	return array->base[0];
}

AT_Node *AT_Array_GetBack(AT_Array *array)
{
	return array->base[(array->size - 1) * sizeof (AT_Node *)];
}

AT_Node *AT_Array_GetAt(AT_Array *array, size_t index)
{
	return array->base[index * sizeof (AT_Node *)];
}

void AT_Array_PushBack(AT_Array *array, AT_Node *data)
{
	array->base[(array->size * sizeof (AT_Node *))] =  data;
	array->size++;
}

void AT_Array_PopBack(AT_Array *array)
{
	array->size--;
}
