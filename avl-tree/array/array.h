#ifndef AVL_TREE_ARRAY_H
#define AVL_TREE_ARRAY_H

#include <stddef.h>

#include "../node/node.h"

typedef struct AT_Array {
	AT_Node **base;
	size_t capacity;
	size_t size;
} AT_Array;

AT_Array *AT_Array_Create(size_t capacity);
void AT_Array_Destroy(AT_Array *array);
AT_Node *AT_Array_GetFront(AT_Array *array);
AT_Node *AT_Array_GetBack(AT_Array *array);
AT_Node *AT_Array_GetAt(AT_Array *array, size_t index);
void AT_Array_PushBack(AT_Array *array, AT_Node *data);
void AT_Array_Insert(AT_Array *array, size_t index, AT_Node *data);
void AT_Array_PopBack(AT_Array *array);

#endif
