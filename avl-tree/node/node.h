#ifndef AVL_TREE_NODE_H
#define AVL_TREE_NODE_H

#include <stddef.h>

typedef struct AT_Node {
	void *data;
	struct AT_Node *parent;
	struct AT_Node *left;
	struct AT_Node *right;
	int balanceFactor;
} AT_Node;

AT_Node *AT_Node_Create(void *data, size_t dataSize);
void AT_Node_Destroy(AT_Node *node);

AT_Node *AT_Node_GetPredecessor(AT_Node *node);
AT_Node *AT_Node_GetSuccessor(AT_Node *node);

#endif
