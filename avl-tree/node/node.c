#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "node.h"

AT_Node *AT_Node_Create(void *data, size_t dataSize)
{
	AT_Node *node = (AT_Node *)malloc(sizeof (AT_Node));
	assert(node);
	node->data = malloc(dataSize);
	assert(node->data);
	memcpy(node->data, data, dataSize);
	node->left = NULL;
	node->right = NULL;
	node->balanceFactor = 0;
	return node;
}

void AT_Node_Destroy(AT_Node *node)
{
	free(node->data);
	free(node);
}

AT_Node *AT_Node_GetPredecessor(AT_Node *node)
{
	AT_Node *nodeLeft = node->left;
	AT_Node *nodeParent = node;
	free(node->data);
	node = nodeLeft;
	while (node->right) {
		node = node->right;
	}
	nodeParent->data = node->data;
	nodeLeft = node->left;
	nodeParent = node->parent;
	nodeParent->right = nodeLeft;
	if (nodeLeft) {
		nodeLeft->parent = nodeParent;
	}
	free(node);
	node = NULL;
	return nodeParent;
}

AT_Node *AT_Node_GetSuccessor(AT_Node *node)
{
	AT_Node *nodeRight = node->right;
	AT_Node *nodeParent = node;
	free(node->data);
	node = nodeRight;
	while (node->left) {
		node = node->left;
	}
	nodeParent->data = node->data;
	nodeRight = node->right;
	nodeParent = node->parent;
	nodeParent->left = nodeRight;
	if (nodeRight) {
		nodeRight->parent = nodeParent;
	}
	free(node);
	node = NULL;
	return nodeParent;
}
