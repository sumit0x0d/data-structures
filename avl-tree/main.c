#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "avl-tree.h"

int compareInt(void *data1, void *data2)
{
	if (*(int *)data1 < *(int *)data2) {
		return -1;
	} else if (*(int *)data1 > *(int *)data2) {
		return 1;
	}
	return 0;
}

void printInt(void *data)
{
	printf("%d ", *(int *)data);
}

int main(void)
{
	srand((int)time(NULL));
	AvlTree *tree = AvlTree_Create(sizeof (int), compareInt);
	printf("%zu\n", sizeof (AvlTree));
	for (int i = 0; i < 10; i++) {
		 int a = rand() % 100;
		 printf("Inserting %d\n", a);
		 AvlTree_Insert(tree, &a);
	}
	AvlTree_TraverseInorder(tree, printInt);
	return 0;
}
