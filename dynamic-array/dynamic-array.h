#ifndef DATA_STRUCTURES_DYNAMIC_ARRAY_H
#define DATA_STRUCTURES_DYNAMIC_ARRAY_H

#include <stddef.h>

typedef struct DynamicArray {
	void *base;
	size_t dataSize;
	size_t capacity;
	double growthFactor;
	size_t size;
	int (*compare)(void *data1, void *data2);
} DynamicArray;

DynamicArray *DynamicArray_Create(size_t dataSize, size_t capacity, double growthFactor,
	int (*compare)(void *data1, void *data2));
void DynamicArray_Destroy(DynamicArray *array);
void *DynamicArray_Front(DynamicArray *array);
void *DynamicArray_Back(DynamicArray *array);
void *DynamicArray_At(DynamicArray *array, size_t index);
void DynamicArray_PushBack(DynamicArray *array, void *data);
void DynamicArray_Insert(DynamicArray *array, size_t index, void *data);
void DynamicArray_PopBack(DynamicArray *array);

#endif
