#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "dynamic-array.h"

static inline void changeCapacity(DynamicArray *array, size_t capacity);

DynamicArray *DynamicArray_Create(size_t dataSize, size_t capacity, double growthFactor,
	int (*compare)(void *data1, void *data2))
{
	assert(growthFactor > 1);
	DynamicArray *array = (DynamicArray *)malloc(sizeof (DynamicArray));
	assert(array);
	array->base = malloc(capacity * dataSize);
	assert(array->base);
	array->dataSize = dataSize;
	array->capacity = capacity;
	array->growthFactor = growthFactor;
	array->compare = compare;
	array->size = 0;
	return array;
}

void DynamicArray_Destroy(DynamicArray *array)
{
	free(array->base);
	free(array);
}

void *DynamicArray_Front(DynamicArray *array)
{
	return (char *)array->base + (array->dataSize * 0);
}

void *DynamicArray_Back(DynamicArray *array)
{
	return (char *)array->base + (array->dataSize * (array->size - 1));
}

void *DynamicArray_At(DynamicArray *array, size_t index)
{
	return (char *)array->base + (array->dataSize * index);
}

void DynamicArray_PushBack(DynamicArray *array, void *data)
{
	if (array->size == array->capacity) {
		changeCapacity(array, array->capacity * array->growthFactor);
	}
	memcpy((char *)array->base + (array->dataSize * array->size), data, array->dataSize);
	array->size++;
}

void DynamicArray_Insert(DynamicArray *array, size_t index, void *data)
{
	if (array->size == array->capacity) {
		changeCapacity(array, array->capacity * array->growthFactor);
	}
	memcpy((char *)array->base + (array->dataSize * index), data, array->dataSize);
	array->size++;
}

void DynamicArray_PopBack(DynamicArray *array)
{
	if (array->size == array->capacity / array->growthFactor) {
		changeCapacity(array, array->capacity / array->growthFactor);
	}
	array->size--;
}

static inline void changeCapacity(DynamicArray *array, size_t capacity)
{
	void *base = realloc(array->base, array->dataSize * array->capacity);
	assert(base);
	array->base = base;
	array->capacity = capacity;
}
