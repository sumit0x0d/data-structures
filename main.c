#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "avl-tree/avl-tree.h"

int compareInt(void *data1, void *data2)
{
	if (*(int *)data1 < *(int *)data2) {
		return -1;
	} else if (*(int *)data1 > *(int *)data2) {
		return 1;
	}
	return 0;
}

void printInt(void *data)
{
	printf("%d ", *(int *)data);
}

int main(void)
{
	srand((int)time(NULL));
	DS_AvlTree *tree = DS_AvlTree_Create(sizeof (int), compareInt);
	printf("%zu\n", sizeof (DS_AvlTree));
	for (int i = 0; i < 10; i++) {
		 int a = rand() % 100;
		 printf("Inserting %d\n", a);
		 DS_AvlTree_Insert(tree, &a);
	}
	DS_AvlTree_TraverseInorder(tree, printInt);
	return 0;
}
