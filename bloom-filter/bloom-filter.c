#include <assert.h>
#include <stdlib.h>

#include "bloom-filter.h"

BloomFilter *BloomFilter_Create(size_t dataSize, size_t bucketCount,
	size_t (*hash)(void *data, size_t bucketCount))
{
	BloomFilter *bloomFilter = (BloomFilter *)malloc(sizeof (BloomFilter));
	assert(bloomFilter);
	bloomFilter->base = (char *)calloc(bucketCount, sizeof (char));
	assert(bloomFilter->base);
	bloomFilter->dataSize = dataSize;
	bloomFilter->bucketCount = bucketCount;
	bloomFilter->size = 0;
	bloomFilter->hash = hash;
	return bloomFilter;
}

void BloomFilter_Destroy(BloomFilter *bloomFilter)
{
	free(bloomFilter->base);
	bloomFilter->base = NULL;
	free(bloomFilter);
}

char *BloomFilter_Search(BloomFilter *bloomFilter, void *data)
{
	size_t index = bloomFilter->hash(data, bloomFilter->bucketCount);
	if (bloomFilter->base[index]) {
		return bloomFilter->base + index;
	}
	return NULL;
}

void BloomFilter_Insert(BloomFilter *bloomFilter, void *data)
{
	size_t index = bloomFilter->hash(data, bloomFilter->bucketCount);
	for (size_t i = 0; i < sizeof (char); i++) {
		(bloomFilter->base + index)[i] = 1;
	}
	bloomFilter->size = bloomFilter->size + 1;
}
