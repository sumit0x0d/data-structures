#ifndef DATA_STRUCTURES_BLOOM_FILTER_H
#define DATA_STRUCTURES_BLOOM_FILTER_H

#include <stddef.h>

typedef struct BloomFilter {
	char *base;
	size_t dataSize;
	size_t bucketCount;
	size_t size;
	size_t (*hash)(void *data, size_t bucketCount);
} BloomFilter;

BloomFilter *BloomFilter_Create(size_t dataSize, size_t bucketCount,
	size_t (*hash)(void *data, size_t bucketCount));
void BloomFilter_Destroy(BloomFilter *bloomFilter);
char *BloomFilter_Search(BloomFilter *bloomFilter, void *data);
void BloomFilter_Insert(BloomFilter *bloomFilter, void *data);

#endif
