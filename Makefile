CC = gcc

TARGET = main

CFLAGS = -O0 -g -Wall -Wpedantic -Wextra -Werror

array:
	$(CC) main.c -o $(TARGET) \
	array/array.c \
	$(CFLAGS)

avl-tree:
	$(CC) main.c -o $(TARGET) \
	array/array.c \
	avl-tree/avl-tree.c \
	avl-tree/node/node.c \
	circular-buffer/circular-buffer.c \
	$(CFLAGS)

bloom-filter:
	$(CC) main.c -o $(TARGET) \
	bloom-filter/bloom-filter.c \
	bloom-filter/node/node.c \
	$(CFLAGS)

double-ended-queue:
	$(CC) main.c -o $(TARGET) \
	double-ended-queue/double-ended-queue.c \
	double-ended-queue/node/node.c \
	$(CFLAGS)

doubly-linked-list:
	$(CC) main.c -o $(TARGET) \
	doubly-linked-list/doubly-linked-list.c \
	doubly-linked-list/node/node.c \
	$(CFLAGS)

dynamic-array:
	$(CC) main.c -o $(TARGET) \
	dynamic-array/dynamic-array.c \
	$(CFLAGS)

hash-table:
	$(CC) main.c -o $(TARGET) \
	hash-table/hash-table.c \
	hash-table/pair/pair.c \
	hash-table/red-black-tree/node/node.c \
	hash-table/red-black-tree/queue/queue.c \
	hash-table/red-black-tree/red-black-tree.c \
	hash-table/red-black-tree/stack/stack.c \
	$(CFLAGS)

linked-list:
	$(CC) main.c -o $(TARGET) \
	linked-list/linked-list.c \
	linked-list/node/node.c \
	$(CFLAGS)

red-black-tree:
	$(CC) main.c -o $(TARGET) \
	array/array.c \
	circular-buffer/circular-buffer.c \
	red-black-tree/node/node.c \
	red-black-tree/red-black-tree.c \
	$(CFLAGS)

trie:
	$(CC) main.c -o $(TARGET) \
	trie/node/node.c \
	trie/trie.c \
	$(CFLAGS)

xor-linked-list:
	$(CC) main.c -o $(TARGET) \
	xor-linked-list/node/node.c \
	xor-linked-list/xor-linked-list.c \
	$(CFLAGS)

clean:
	rm $(TARGET)
