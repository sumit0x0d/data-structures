#include <assert.h>
#include <stdlib.h>

#include "doubly-linked-list.h"

#include "node/node.h"

DoublyLinkedList *DoublyLinkedList_Create(size_t dataSize)
{
	DoublyLinkedList *linkedList = (DoublyLinkedList *)malloc(sizeof (DoublyLinkedList));
	assert(linkedList);

	linkedList->head = NULL;
	linkedList->tail = NULL;
	linkedList->dataSize = dataSize;
	linkedList->size = 0;

	return linkedList;
}

DLL_Node *DoublyLinkedList_GetHead(DoublyLinkedList *linkedList)
{
	return linkedList->head;
}

DLL_Node *DoublyLinkedList_GetTail(DoublyLinkedList *linkedList)
{
	return linkedList->tail;
}

void DoublyLinkedList_PushHead(DoublyLinkedList *linkedList, void *data)
{
	DLL_Node *node = DLL_Node_Create(data, linkedList->dataSize);
	node->previous = NULL;
	if (linkedList->size) {
		linkedList->head->previous = node;
		node->next = linkedList->head;
	} else {
		linkedList->tail = node;
		node->next = NULL;
	}
	linkedList->head = node;
	linkedList->size++;
}

void DoublyLinkedList_PushTail(DoublyLinkedList *linkedList, void *data)
{
	DLL_Node *node = DLL_Node_Create(data, linkedList->dataSize);
	node->next = NULL;
	if (linkedList->size) {
		linkedList->tail->next = node;
		node->previous = linkedList->tail;
	} else {
		linkedList->head = node;
		node->previous = NULL;
	}
	linkedList->tail = node;
	linkedList->size++;
}

void DoublyLinkedList_PopHead(DoublyLinkedList *linkedList)
{
	DLL_Node *node = linkedList->head;
	linkedList->head = linkedList->head->next;
	if (!linkedList->head) {
		linkedList->tail = NULL;
	}
	DLL_Node_Destroy(node);
	linkedList->size--;
}

void DoublyLinkedList_PopTail(DoublyLinkedList *linkedList)
{
	DLL_Node *node = linkedList->tail;
	linkedList->tail = linkedList->tail->previous;
	if (linkedList->tail) {
		linkedList->tail->next = NULL;
	}
	DLL_Node_Destroy(node);
	linkedList->size--;
}

void DoublyLinkedList_Remove(DoublyLinkedList *linkedList, DLL_Node *node)
{
	if (node->previous && node->previous->next == (DLL_Node *)node) {
		node->previous->next = node->next;
	} else {
		linkedList->head = linkedList->head->next;
	}
	if (node->next && node->next->previous == (DLL_Node *)node) {
		node->next->previous = node->previous;
	} else {
		linkedList->tail = linkedList->tail->previous;
	}
	DLL_Node_Destroy(node);
	linkedList->size--;
}

void DoublyLinkedList_Traverse(DoublyLinkedList *linkedList, void (*function)(void *data))
{
	DLL_Node *node = linkedList->head;
	while (node) {
		function(node->data);
		node = node->next;
	}
}
