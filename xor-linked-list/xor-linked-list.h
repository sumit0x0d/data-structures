#ifndef XOR_LINKED_LIST_H
#define XOR_LINKED_LIST_H

#include <stddef.h>

#include "node/node.h"

typedef struct XorLinkedList {
	XLL_Node *head;
	XLL_Node *tail;
	size_t dataSize;
	size_t size;
} XorLinkedList;

XorLinkedList *XorLinkedList_Create(size_t dataSize);
void XorLinkedList_Destroy(XorLinkedList *linkedList);
XLL_Node *XorLinkedList_Head(XorLinkedList *linkedList);
XLL_Node *XorLinkedList_Tail(XorLinkedList *linkedList);
void XorLinkedList_PushFront(XorLinkedList *linkedList, void *data);
void XorLinkedList_PushBack(XorLinkedList *linkedList, void *data);
void XorLinkedList_Insert(XorLinkedList *linkedList, size_t index, void *data);
void XorLinkedList_PopFront(XorLinkedList *linkedList);
void XorLinkedList_PopBack(XorLinkedList *linkedList);
void XorLinkedList_Remove(XorLinkedList *linkedList, void *data);
void XorLinkedList_Erase(XorLinkedList *linkedList, size_t index);
void XorLinkedList_Update(XorLinkedList *linkedList, size_t index, void *data);

#endif
