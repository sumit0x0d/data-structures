#include <assert.h>
#include <stdlib.h>

#include "xor-linked-list.h"

#include "node/node.h"

XorLinkedList *XorLinkedList_Create(size_t dataSize)
{
	XorLinkedList *linkedList = (XorLinkedList *)malloc(sizeof (XorLinkedList));
	assert(linkedList);
	linkedList->head = NULL;
	linkedList->tail = NULL;
	linkedList->dataSize = dataSize;
	linkedList->size = 0;
	return linkedList;
}

void *XorLinkedList_GetHead(XorLinkedList *linkedList)
{
	return linkedList->head->data;
}

void *XorLinkedList_Back(XorLinkedList *linkedList)
{
	return linkedList->tail->data;
}

void XorLinkedList_PushHead(XorLinkedList *linkedList, void *data)
{
	XLL_Node *node = XLL_Node_Create(data, linkedList->dataSize);
	node->xor = 0 ^ (size_t)linkedList->head;
	linkedList->head = node;
	if (!linkedList->size) {
		linkedList->tail = node;
	}
	linkedList->size++;
}

void XorLinkedList_PushTail(XorLinkedList *linkedList, void *data)
{
	XLL_Node *node = XLL_Node_Create(data, linkedList->dataSize);
	node->xor = (size_t)linkedList->tail ^ 0;
	linkedList->tail = node;
	if (!linkedList->size) {
		linkedList->head = node;
	}
	linkedList->size++;
}

// void XorLinkedList_Insert(XorLinkedList *linkedList, size_t index, void *data)
// {

// }

// void XorLinkedList_SortedInsert(XorLinkedList *linkedList, void *data)
// {

// }

// void XorLinkedListPopead(XorLinkedList *linkedList)
// {

// }

// void XorLinkedListPop_Tail(XorLinkedList *linkedList)
// {

// }

// void XorLinkedList_Erase(XorLinkedList *linkedList, size_t index)
// {

// }

// void XorLinkedList_Remove(XorLinkedList *linkedList, void *data)
// {

// }
