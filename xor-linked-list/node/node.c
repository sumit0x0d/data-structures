#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "node.h"

XLL_Node *XLL_Node_Create(void *data, size_t dataSize)
{
	XLL_Node *node = (XLL_Node *)malloc(sizeof (XLL_Node));
	assert(node);
	node->data = malloc(dataSize);
	assert(node);
	memcpy(node->data, data, dataSize);
	return node;
}

void XLL_Node_Destroy(XLL_Node *node)
{
	free(node->data);
	free(node);
}
