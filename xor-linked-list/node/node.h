#ifndef XOR_LINKED_LIST_NODE_H
#define XOR_LINKED_LIST_NODE_H

#include <stddef.h>

typedef struct XLL_Node {
	void *data;
	size_t xor;
} XLL_Node;

XLL_Node *XLL_Node_Create(void *data, size_t dataSize);
void XLL_Node_Destroy(XLL_Node *node);

#endif
