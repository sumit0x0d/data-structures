#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "circular-buffer.h"

CircularBuffer *CircularBuffer_Create(size_t dataSize, size_t capacity)
{
	CircularBuffer *array = (CircularBuffer *)malloc(sizeof (CircularBuffer));
	assert(array);
	array->base = malloc(capacity * sizeof (dataSize));
	assert(array->base);
	array->front = 0;
	array->back = 0;
	array->dataSize = dataSize;
	array->capacity = capacity;
	array->size = 0;
	return array;
}

void CircularBuffer_Destroy(CircularBuffer *array)
{
	free(array->base);
	free(array);
}

void *CircularBuffer_GetFront(CircularBuffer *array)
{
	return (char *)array->base + (array->dataSize * array->front);
}

void *CircularBuffer_GetBack(CircularBuffer *array)
{
	if (array->back == 0) {
		return (char *)array->base + (array->dataSize * (array->capacity - 1));
	}
	return (char *)array->base + (array->dataSize * (array->back - 1));
}

void CircularBuffer_PushBack(CircularBuffer *array, void *data)
{
	memcpy((char *)array->base + (array->dataSize * array->back), data, array->dataSize);
	array->back = (array->back + 1) % array->capacity;
	array->size++;
}

void CircularBuffer_PopFront(CircularBuffer *array)
{
	array->front = (array->front + 1) % array->capacity;
	array->size--;
}
