#ifndef DATA_STRUCTURES_CIRCULAR_BUFFER_H
#define DATA_STRUCTURES_CIRCULAR_BUFFER_H

#include <stddef.h>

typedef struct CircularBuffer {
	void *base;
	size_t dataSize;
	size_t front;
	size_t back;
	size_t capacity;
	size_t size;
} CircularBuffer;

CircularBuffer *CircularBuffer_Create(size_t dataSize, size_t capacity);
void CircularBuffer_Destroy(CircularBuffer *array);
void *CircularBuffer_GetFront(CircularBuffer *array);
void *CircularBuffer_GetBack(CircularBuffer *array);
void CircularBuffer_PushBack(CircularBuffer *array, void *data);
void CircularBuffer_PopFront(CircularBuffer *array);

#endif
