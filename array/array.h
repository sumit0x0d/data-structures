#ifndef DATA_STRUCTURES_ARRAY_H
#define DATA_STRUCTURES_ARRAY_H

#include <stddef.h>

typedef struct Array {
	void *base;
	size_t dataSize;
	size_t capacity;
	size_t size;
} Array;

Array *Array_Create(size_t dataSize, size_t capacity);
void Array_Destroy(Array *array);
void *Array_GetFront(Array *array);
void *Array_GetBack(Array *array);
void *Array_GetAt(Array *array, size_t index);
void Array_PushBack(Array *array, void *data);
void Array_Insert(Array *array, size_t index, void *data);
void Array_PopBack(Array *array);

#endif
