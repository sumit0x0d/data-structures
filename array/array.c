#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "array.h"

Array *Array_Create(size_t dataSize, size_t capacity)
{
	Array *array = (Array *)malloc(sizeof (Array));
	assert(array);
	array->base = (void *)malloc(capacity * dataSize);
	assert(array->base);
	array->dataSize = dataSize;
	array->capacity = capacity;
	array->size = 0;
	return array;
}

void Array_Destroy(Array *array)
{
	free(array->base);
	free(array);
}

void *Array_GetFront(Array *array)
{
	return (char *)array->base + (array->dataSize * 0);
}

void *Array_GetBack(Array *array)
{
	return (char *)array->base + (array->dataSize * (array->size - 1));
}

void *Array_GetAt(Array *array, size_t index)
{
	return (char *)array->base + (array->dataSize * index);
}

void Array_PushBack(Array *array, void *data)
{
	memcpy((char *)array->base + (array->dataSize * array->size), data, array->dataSize);
	array->size++;
}

void Array_PopBack(Array *array)
{
	array->size--;
}
