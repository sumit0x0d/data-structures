#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "linked-list.h"

#include "node/node.h"

LinkedList *LinkedList_Create(size_t dataSize)
{
	LinkedList *linkedList = (LinkedList *)malloc(sizeof (LinkedList));
	assert(linkedList);

	linkedList->head = NULL;
	linkedList->tail = NULL;
	linkedList->dataSize = dataSize;
	linkedList->size = 0;

	return linkedList;
}

void LinkedList_Destroy(LinkedList *linkedList)
{
	free(linkedList->head);
	free(linkedList);
}

LL_Node *LinkedList_Head(LinkedList *linkedList)
{
	return linkedList->head;
}

LL_Node *LinkedList_Tail(LinkedList *linkedList)
{
	return linkedList->tail;
}

void LinkedList_PushHead(LinkedList *linkedList, void *data)
{
	LL_Node *node = LL_Node_Create(data, linkedList->dataSize);
	if (linkedList->size) {
		node->next = linkedList->head;
	} else {
		node->next = NULL;
		linkedList->tail = node;
	}
	linkedList->head = node;
	linkedList->size++;
}

void LinkedList_PushTail(LinkedList *linkedList, void *data)
{
	LL_Node *node = LL_Node_Create(data, linkedList->dataSize);
	node->next = NULL;
	if (linkedList->size) {
		linkedList->tail->next = node;
	} else {
		linkedList->head = node;
	}
	linkedList->tail = node;
	linkedList->size++;
}

void LinkedList_PopHead(LinkedList *linkedList)
{
	LL_Node *node = linkedList->head;
	linkedList->head = linkedList->head->next;
	if (!linkedList->head) {
		linkedList->tail = NULL;
	}
	LL_Node_Destroy(node);
	linkedList->size--;
}

void LinkedList_PopTail(LinkedList *linkedList)
{
	if (linkedList->head == linkedList->tail) {
		free(linkedList->head);
		linkedList->head = NULL;
		linkedList->tail = NULL;
		return;
	}
	LL_Node *node = linkedList->head;
	while (node->next != linkedList->tail) {
		node = node->next;
	}
	node->next = NULL;
	free(linkedList->tail);
	linkedList->tail = node;
	linkedList->size--;
}

// int LinkedList_remove(LinkedList *linkedList, void *data)
// {
// LL_Node *node = linkedList->head;
// size_t count = 0;

// while (node) {
// count = count + 1;
// if(node->data == data)
// LinkedList_erase(linkedList, count);
// }

// linkedList->size--;

// return 1;
// }


void LinkedList_Traverse(LinkedList *linkedList, void (*function)(void *data))
{
    LL_Node *node = linkedList->head;
    while (node) {
        function(node->data);
        node = node->next;
    }
}
