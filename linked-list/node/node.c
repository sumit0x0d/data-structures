#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "node.h"

LL_Node *LL_Node_Create(void *data, size_t dataSize)
{
	LL_Node *node = (LL_Node *)malloc(sizeof (LL_Node));
	assert(node);
	node->data = malloc(dataSize);
	assert(node);
	memcpy(node->data, data, dataSize);
	return node;
}

void LL_Node_Destroy(LL_Node *node)
{
	free(node->data);
	free(node);
}
