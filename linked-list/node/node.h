#ifndef LINKED_LIST_NODE_H
#define LINKED_LIST_NODE_H

#include <stddef.h>

typedef struct LL_Node {
	void *data;
	struct LL_Node *next;
} LL_Node;

LL_Node *LL_Node_Create(void *data, size_t dataSize);
void LL_Node_Destroy(LL_Node *node);

#endif
