#ifndef LINKED_LIST_H
#define LINKED_LIST_H

#include <stddef.h>

#include "node/node.h"

typedef struct LinkedList {
	LL_Node *head;
	LL_Node *tail;
	size_t dataSize;
	size_t size;
} LinkedList;

LinkedList *LinkedList_Create(size_t dataSize);
void LinkedList_Destroy(LinkedList *linkedList);
LL_Node *LinkedList_Head(LinkedList *linkedList);
LL_Node *LinkedList_Tail(LinkedList *linkedList);
void LinkedList_PushHead(LinkedList *linkedList, void *data);
void LinkedList_PushTail(LinkedList *linkedList, void *data);
void LinkedList_PopHead(LinkedList *linkedList);
void LinkedList_PopTail(LinkedList *linkedList);
void LinkedList_Traverse(LinkedList *linkedList, void (*function)(void *data));

#endif
