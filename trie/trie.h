#ifndef DATA_STRUCTURES_TRIE_H
#define DATA_STRUCTURES_TRIE_H

#include <stdbool.h>
#include <stddef.h>

typedef struct DS_Trie {
	struct Node *root;
	size_t size;
} DS_Trie;

DS_Trie *trie_Create();
char *trie_Search(DS_Trie *trie, char *string);
int trie_Insert(DS_Trie *trie, char *string);
int trie_Remove(DS_Trie *trie, char *string);

#endif
