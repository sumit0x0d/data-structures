#ifndef DATA_STRUCTURES_NODE_H
#define DATA_STRUCTURES_NODE_H

#include <stdlib.h>

typedef struct DS_Node {
	int terminal;
	struct TrieDS_Node *children[128];
} DS_Node;

DS_Node *DS_Node_Create();

#endif
