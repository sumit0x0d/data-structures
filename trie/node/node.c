#include <assert.h>
#include <stdlib.h>

#include "node.h"

Node *Node_Create()
{
	Node *node = (Node *)malloc(sizeof (Node));
	assert(node);
	node->terminal = 0;
	for (int i = 0; i < 128; i++) {
		node->children[i] = NULL;
	}
	// memset(node->children, '\0', sizeof (Node) * 128);
	return node;
}
