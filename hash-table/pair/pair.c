#include "pair.h"

#include <assert.h>
#include <stdlib.h>
#include <string.h>

HT_Pair *HT_Pair_Create(void *key, size_t keySize, void *value, size_t valueSize)
{
	HT_Pair *pair = (HT_Pair *)malloc(sizeof (HT_Pair));
	assert(pair);
	pair->key = malloc(keySize);
	assert(pair->key);
	memcpy(pair->key, key, keySize);
	pair->value = malloc(valueSize);
	assert(pair->value);
	memcpy(pair->value, value, valueSize);
	pair->next = NULL;
	return pair;
}

void HT_Pair_Destroy(HT_Pair *pair)
{
	free(pair->key);
	free(pair->value);
	free(pair);
}
