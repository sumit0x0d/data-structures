#ifndef HASH_TABLE_PAIR_H
#define HASH_TABLE_PAIR_H

#include <stddef.h>

typedef struct HT_Pair {
	void *key;
	void *value;
	struct HT_Pair *next;
} HT_Pair;

HT_Pair* HT_Pair_Create(void *key, size_t keySize, void *value, size_t valueSize);
void HT_Pair_Destroy(HT_Pair *pair);

#endif
