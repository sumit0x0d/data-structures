#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "node.h"

RBT_Node *RBT_Node_Create(void *data, size_t dataSize)
{
	RBT_Node *node = (RBT_Node *)malloc(sizeof (RBT_Node));
	assert(node);
	node->data = malloc(dataSize);
	assert(node->data);
	memcpy(node->data, data, dataSize);
	node->left = NULL;
	node->right = NULL;
	return node;
}

void RBT_Node_Destroy(RBT_Node *node)
{
	free(node->data);
	free(node);
}
