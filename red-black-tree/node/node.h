#ifndef RED_BLACK_TREE_NODE_H
#define RED_BLACK_TREE_NODE_H

#include <stddef.h>

typedef struct RBT_Node {
	void *data;
	struct RBT_Node *parent;
	struct RBT_Node *left;
	struct RBT_Node *right;
	enum {
		RED,
		BLACK
	} color;
} RBT_Node;

RBT_Node *RBT_Node_Create(void *data, size_t dataSize);
void RBT_Node_Destroy(RBT_Node *node);

#endif
