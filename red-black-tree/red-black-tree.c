#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "red-black-tree.h"

#include "array/array.h"
#include "circular-buffer/circular-buffer.h"
#include "node/node.h"

static void rotateRight(RedBlackTree *tree, RBT_Node *node);
static void rotateLeftRight(RedBlackTree *tree, RBT_Node *node);
static void rotateLeft(RedBlackTree *tree, RBT_Node *node);
static void rotateRightLeft(RedBlackTree *tree, RBT_Node *node);
static void rebalance(RedBlackTree *tree, RBT_Node *node);

RedBlackTree *RedBlackTree_Create(size_t dataSize, int (*compare)(void *data1, void *data2))
{
	RedBlackTree *tree = (RedBlackTree *)malloc(sizeof (RedBlackTree));
	assert(tree);

	tree->root = NULL;
	tree->dataSize = dataSize;
	tree->size = 0;
	tree->compare = compare;

	return tree;
}

void RedBlackTree_Destroy(RedBlackTree *tree)
{
	RBT_Node *node = tree->root;
	RBT_CircularBuffer *circularBuffer = RBT_CircularBuffer_Create(tree->size);
	free(node->data);
	RBT_CircularBuffer_PushBack(circularBuffer, node);
	while (circularBuffer->size) {
		node = (RBT_Node *)RBT_CircularBuffer_GetFront(circularBuffer);
		RBT_CircularBuffer_PopFront(circularBuffer);
		if (node->left) {
			free(node->data);
			RBT_CircularBuffer_PushBack(circularBuffer, node->left);
		}
		if (node->right) {
			free(node->data);
			RBT_CircularBuffer_PushBack(circularBuffer, node->right);
		}
	}
	RBT_CircularBuffer_Destroy(circularBuffer);
	free(tree);
}

RBT_Node *RedBlackTree_Root(RedBlackTree *tree)
{
	return tree->root;
}

RBT_Node *RedBlackTree_Search(RedBlackTree *tree, void *data)
{
	RBT_Node *node = tree->root;
	while (node) {
		int compare = tree->compare(data, node->data);
		if (compare == 0) {
			return node;
		} else if (compare < 0) {
			node = node->left;
		} else {
			node = node->right;
		}
	}
	return NULL;
}

void RedBlackTree_Insert(RedBlackTree *tree, void *data)
{
	if (!tree->root) {
		tree->root = RBT_Node_Create(data, tree->dataSize);
		tree->root->parent = NULL;
		tree->root->color = 1;
		tree->size++;
		return;
	}
	RBT_Node *node = tree->root;
	RBT_Node *nodeParent = NULL;
	int compare;
	while (node) {
		compare = tree->compare(data, node->data);
		if (compare == 0) {
			return;
		}
		nodeParent = node;
		if (compare < 0) {
			node = node->left;
		} else {
			node = node->right;
		}
	}
	node = RBT_Node_Create(data, tree->dataSize);
	node->parent = nodeParent;
	node->color = 0;
	compare = tree->compare(data, nodeParent->data);
	if (compare < 0) {
		nodeParent->left = node;
	} else {
		nodeParent->right = node;
	}
	rebalance(tree, nodeParent);
	tree->size++;
}

// void RedBlackTree_remove(RedBlackTree *tree, int data)
// {

// }

void RedBlackTree_TraversePreorder(RedBlackTree *tree, void (*function)(void *data))
{
	RBT_Node *node = tree->root;
	RBT_Array *array = RBT_Array_Create(tree->size);
	while (node || array->size) {
		if (node) {
			RBT_Array_PushBack(array, node);
			function(node->data);
			node = node->left;
		} else {
			node = (RBT_Node *)RBT_Array_Back(array);
			RBT_Array_PopBack(array);
			node = node->right;
		}
	}
	RBT_Array_Destroy(array);
}

void RedBlackTree_TraverseInorder(RedBlackTree *tree, void (*function)(void *data))
{
	RBT_Node *node = tree->root;
	RBT_Array *array = RBT_Array_Create(tree->size);
	while (node || array->size) {
		if (node) {
			RBT_Array_PushBack(array, node);
			node = node->left;
		} else {
			node = (RBT_Node *)RBT_Array_Back(array);
			RBT_Array_PopBack(array);
			function(node->data);
			node = node->right;
		}
	}
	RBT_Array_Destroy(array);
}

// static void preorder_Traverse(RedBlackTree *tree)
// {
//	 if (RedBlackTree_empty(tree)) {
//		 return;
//	 }
//	 RBT_Node *node = tree->root;
//	 RBT_ArrayLL* S = RBT_ArrayLL_Create(sizeof (RedBlackTreeRBT_Node));
//	 while (node || S->size) {
//		 if (node) {
//			 RBT_ArrayLL_Push(S, node);
//			 printf("%d(%d) ", *(int*)node->data, node->balance_factor);
//			 node = node->left;
//		 }
//		 else {
//			 node = S->top->data;
//			 RBT_ArrayLLPop(S);
//			 node = node->right;
//		 }
//	 }
// }

void RedBlackTree_TraverseLevelorder(RedBlackTree *tree, void (*function)(void *data))
{
	RBT_Node *node = tree->root;
	RBT_CircularBuffer *circularBuffer = RBT_CircularBuffer_Create(tree->size);
	function(node->data);
	RBT_CircularBuffer_PushBack(circularBuffer, node);
	while (circularBuffer->size) {
		node = (RBT_Node *)RBT_CircularBuffer_GetFront(circularBuffer);
		RBT_CircularBuffer_PopFront(circularBuffer);
		if (node->left) {
			function(node->data);
			RBT_CircularBuffer_PushBack(circularBuffer, node->left);
		}
		if (node->right) {
			function(node->data);
			RBT_CircularBuffer_PushBack(circularBuffer, node->right);
		}
	}
	RBT_CircularBuffer_Destroy(circularBuffer);
}

static void rotateRight(RedBlackTree *tree, RBT_Node *node)
{
	RBT_Node *nodeLeft = node->left;
	node->left = nodeLeft->right;
	if (node->left) {
		node->left->parent = node;
	}
	nodeLeft->parent = node->parent;
	if (nodeLeft->parent) {
		if (nodeLeft->parent->left == node) {
			nodeLeft->parent->left = nodeLeft;
		} else {
			nodeLeft->parent->right = nodeLeft;
		}
	} else {
		tree->root = nodeLeft;
	}
	node->parent = nodeLeft;
	nodeLeft->right = node;
}

static void rotateLeftRight(RedBlackTree *tree, RBT_Node *node)
{
	RBT_Node *nodeLeft = node->left;
	RBT_Node *nodeLeftRight = node->left->right;
	nodeLeft->right = nodeLeftRight->left;
	if (nodeLeft->right) {
		nodeLeft->right->parent = nodeLeft;
	}
	node->left = nodeLeftRight->right;
	if (node->left) {
		node->left->parent = node;
	}
	nodeLeftRight->left = nodeLeft;
	nodeLeft->parent = nodeLeftRight;
	nodeLeftRight->parent = node->parent;
	if (nodeLeftRight->parent) {
		if (nodeLeftRight->parent->left == node) {
			nodeLeftRight->parent->left = nodeLeftRight;
		} else {
			nodeLeftRight->parent->right = nodeLeftRight;
		}
	} else {
		tree->root = nodeLeftRight;
	}
	node->parent = nodeLeftRight;
	nodeLeftRight->right = node;
}

static void rotateLeft(RedBlackTree *tree, RBT_Node *node)
{
	RBT_Node *nodeRight = node->right; 
	node->right = nodeRight->left;
	if (node->right) {
		node->right->parent = node;
	}
	nodeRight->parent = node->parent;
	if (nodeRight->parent) {
		if (nodeRight->parent->left == node) {
			nodeRight->parent->left = nodeRight;
		} else {
			nodeRight->parent->right = nodeRight;
		}
	} else {
		tree->root = nodeRight;
	}
	node->parent = nodeRight;
	nodeRight->left = node;
}

static void rotateRightLeft(RedBlackTree *tree, RBT_Node *node)
{
	RBT_Node *nodeRight = node->right;
	RBT_Node *nodeRightLeft = node->right->left;
	nodeRight->left = nodeRightLeft->right;
	if (nodeRight->left) {
		nodeRight->left->parent = nodeRight;
	}
	node->right = nodeRightLeft->left;
	if (node->right) {
		node->right->parent = node;
	}
	nodeRightLeft->right = nodeRight;
	nodeRight->parent = nodeRightLeft;
	nodeRightLeft->parent = node->parent;
	if (nodeRightLeft->parent) {
		if (nodeRightLeft->parent->left == node) {
			nodeRightLeft->parent->left = nodeRightLeft;
		} else {
			nodeRightLeft->parent->right = nodeRightLeft;
		}
	} else {
		tree->root = nodeRightLeft;
	}
	node->parent = nodeRightLeft;
	nodeRightLeft->left = node;
}

static void rebalance(RedBlackTree *tree, RBT_Node *node)
{
	while (node) {
		if (node->color == 0) {
			if (node->parent->left == node) {
				if (!node->parent->right || node->parent->right->color == 1) {
					rotateRight(tree, node);
				}
				else if (node->parent->right || node->parent->right->color == 0) {

				}
			}
		}
		if (node->parent->left == node) {
			if (node->color == 0 && (!node->parent->right || node->parent->right->color == 1)) {
			}
			if (node->color == 0 && (node->parent->right && node->parent->right->color == 0)) {
				node->parent->color = 1;
			}
		} else {
			if (node->color == 0 && (!node->parent->left || node->parent->left->color == 1)) {
				rotateLeft(tree, node);
				rotateRightLeft(tree, node);
				rotateLeftRight(tree, node);
				rotateRight(tree, node);
			}
			if (node->color == 0 && (node->parent->left && node->parent->left->color == 0)) {
				node->parent->color = 1;
			}
		}
		tree->root = node->parent;
	}
}
