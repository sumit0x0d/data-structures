#ifndef RED_BLACK_TREE_CIRCULAR_BUFFER_H
#define RED_BLACK_TREE_CIRCULAR_BUFFER_H

#include <stddef.h>

#include "../node/node.h"

typedef struct RBT_CircularBuffer {
	RBT_Node **base;
	size_t dataSize;
	size_t front;
	size_t back;
	size_t capacity;
	size_t size;
} RBT_CircularBuffer;

RBT_CircularBuffer *RBT_CircularBuffer_Create(size_t capacity);
void RBT_CircularBuffer_Destroy(RBT_CircularBuffer *array);
RBT_Node *RBT_CircularBuffer_GetFront(RBT_CircularBuffer *array);
RBT_Node *RBT_CircularBuffer_GetBack(RBT_CircularBuffer *array);
void RBT_CircularBuffer_PushBack(RBT_CircularBuffer *array, RBT_Node *data);
void RBT_CircularBuffer_PopFront(RBT_CircularBuffer *array);

#endif
