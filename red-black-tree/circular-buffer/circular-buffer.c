#include <assert.h>
#include <stdlib.h>

#include "circular-buffer.h"

RBT_CircularBuffer *RBT_CircularBuffer_Create(size_t capacity)
{
	RBT_CircularBuffer *array = (RBT_CircularBuffer *)malloc(sizeof (RBT_CircularBuffer));
	array->base = (RBT_Node **)malloc(capacity * sizeof (RBT_Node *));
	assert(array->base);
	array->front = 0;
	array->back = 0;
	array->capacity = capacity;
	array->size = 0;
	return array;
}

void RBT_CircularBuffer_Destroy(RBT_CircularBuffer *array)
{
	free(array->base);
}

RBT_Node *RBT_CircularBuffer_GetFront(RBT_CircularBuffer *array)
{
	return array->base[array->front * sizeof (RBT_Node *)];
}

RBT_Node *RBT_CircularBuffer_GetBack(RBT_CircularBuffer *array)
{
	if (array->back == 0) {
		return array->base[(array->capacity - 1) * sizeof (RBT_Node *)];
	}
	return array->base[(array->back - 1) * sizeof (RBT_Node *)];
}

void RBT_CircularBuffer_PushBack(RBT_CircularBuffer *array, RBT_Node *data)
{
	array->base[array->back * sizeof (RBT_Node *)] = data;
	array->back = (array->back + 1) % array->capacity;
	array->size++;
}

void RBT_CircularBuffer_PopFront(RBT_CircularBuffer *array)
{
	array->front = (array->front + 1) % array->capacity;
	array->size--;
}
