#ifndef DATA_STRUCTURES_RED_BLACK_TREE_H
#define DATA_STRUCTURES_RED_BLACK_TREE_H

#include <stddef.h>

#include "node/node.h"

typedef struct RedBlackTree {
	RBT_Node *root;
	size_t dataSize;
	size_t size;
	int (*compare)(void *data1, void *data2);
} RedBlackTree;

RedBlackTree *RedBlackTree_Create(size_t dataSize, int (*compare)(void *data1, void *data2));
void RedBlackTree_Destroy(RedBlackTree *tree);
RBT_Node *RedBlackTree_Root(RedBlackTree *tree);
RBT_Node *RedBlackTree_Search(RedBlackTree *tree, void *data);
void RedBlackTree_Insert(RedBlackTree *tree, void *data);
void RedBlackTree_Remove(RedBlackTree *tree, void *data);
void RedBlackTree_TraversePreorder(RedBlackTree *tree, void (*function)(void *data));
void RedBlackTree_TraverseInorder(RedBlackTree *tree, void (*function)(void *data));
void RedBlackTree_TraversePostorder(RedBlackTree *tree, void (*function)(void *data));
void RedBlackTree_TraverseLevelorder(RedBlackTree *tree, void (*function)(void *data));

#endif
