#include <assert.h>
#include <stdlib.h>

#include "array.h"

RBT_Array *RBT_Array_Create(size_t capacity)
{
	RBT_Array *array = (RBT_Array *)malloc(sizeof (RBT_Array));
	assert(array);
	array->base = (RBT_Node **)malloc(capacity * sizeof (RBT_Node *));
	assert(array->base);
	array->capacity = capacity;
	array->size = 0;
	return array;
}

void RBT_Array_Destroy(RBT_Array *array)
{
	free(array->base);
	free(array);
}

RBT_Node *RBT_Array_GetFront(RBT_Array *array)
{
	return array->base[0];
}

RBT_Node *RBT_Array_GetBack(RBT_Array *array)
{
	return array->base[(array->size - 1) * sizeof (RBT_Node *)];
}

RBT_Node *RBT_Array_GetAt(RBT_Array *array, size_t index)
{
	return array->base[index * sizeof (RBT_Node *)];
}

void RBT_Array_PushBack(RBT_Array *array, RBT_Node *data)
{
	array->base[(array->size * sizeof (RBT_Node *))] =  data;
	array->size++;
}

void RBT_Array_PopBack(RBT_Array *array)
{
	array->size--;
}
