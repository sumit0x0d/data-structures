#ifndef RED_BLACK_TREE_ARRAY_H
#define RED_BLACK_TREE_ARRAY_H

#include <stddef.h>

#include "../node/node.h"

typedef struct RBT_Array {
	RBT_Node **base;
	size_t dataSize;
	size_t capacity;
	size_t size;
} RBT_Array;

RBT_Array *RBT_Array_Create(size_t capacity);
void RBT_Array_Destroy(RBT_Array *array);
RBT_Node *RBT_Array_Front(RBT_Array *array);
RBT_Node *RBT_Array_Back(RBT_Array *array);
RBT_Node *RBT_Array_At(RBT_Array *array, size_t index);
void RBT_Array_PushBack(RBT_Array *array, RBT_Node *data);
void RBT_Array_Insert(RBT_Array *array, size_t index, RBT_Node *data);
void RBT_Array_PopBack(RBT_Array *array);

#endif
